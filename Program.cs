﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace ProductionSystem
{
	public class Program
	{
		static void Main()
		{
			var ps = new ProductionSystem(2, 2, new HighSpeedProductionSystemFactory());
			ps.StartProduction();
		}
	}

	public abstract class ProductionSystemFactory
	{
		public abstract Robot CreateRobot();
		public abstract Conveyor CreateConveyor();
	}

	public class HighSpeedProductionSystemFactory : ProductionSystemFactory
	{
		public HighSpeedProductionSystemFactory()
		{
            Console.WriteLine("Создана высокопроизводительная производственная система");
        }

		public override Robot CreateRobot()
		{
			Console.WriteLine("Создан прокачанный робот");
			return new ModifiedRobot();
		}

		public override Conveyor CreateConveyor()
		{
			Console.WriteLine("Создан прокачанный конвейер");
			return new ModifiedConveyor();
		}
	}

	public class LowSpeedProductionSystemFactory : ProductionSystemFactory
	{
		public LowSpeedProductionSystemFactory()
		{
			Console.WriteLine("Создана низкопроизводительная производственная система");
		}

		public override Robot CreateRobot()
		{
			Console.WriteLine("Создан обычный робот");
			return new DeafaultRobot();
		}

		public override Conveyor CreateConveyor()
		{
			Console.WriteLine("Создан обычный конвейер");
			return new DeafaultConveyor();
		}
	}

	public class ProductionSystem : ISubject, IObserver
	{

		private readonly List<Robot> robots = new();
		private readonly List<Conveyor> conveyors = new();
		private readonly Sensor sensor = new();
		public bool IsWorking { get; private set; }

		public ProductionSystem(int robotCount, int conveyorCount, ProductionSystemFactory productionSystemFactory)
		{
			for (int i = 0; i < robotCount; i++)
			{
				var robot = productionSystemFactory.CreateRobot();
				robots.Add(robot);
			}
			for (int i = 0; i < conveyorCount; i++)
			{
				var conveyor = productionSystemFactory.CreateConveyor();
				conveyors.Add(conveyor);
			}
			sensor.Attach(this);
		}

		public void StartProduction()
		{
			Console.WriteLine("Инициирован запуск производства");
			IsWorking = true;
			Notify();
			Console.WriteLine("Производство запущено");
			sensor.StartChecking();
		}

		public void StopProduction()
		{
			Console.WriteLine("Инициирована остановка производства");
			IsWorking = false;
			Notify();
		}

		public void Attach(IObserver observer)
		{
			if (observer is Robot robot) robots.Add(robot);
			if (observer is Conveyor conveyor) conveyors.Add(conveyor);
		}

		public void Detach(IObserver observer)
		{
			if (observer is Robot robot) robots.Add(robot);
			if (observer is Conveyor conveyor) conveyors.Add(conveyor);
		}

		public void Notify()
		{
			foreach (var robot in robots)
				robot.Update(this);
			foreach (var conveyor in conveyors)
				conveyor.Update(this);
		}

		public void Update(ISubject subject)
		{
			if (subject is Sensor s)
			{
				if (s.IsWorkableTempreture == false)
				{
					Console.WriteLine($"Система была уведомлена о превышении рабочей температуры");
					StopProduction();
				}
			}
		}
	}

	public class Sensor : ISubject
	{
		private readonly List<IObserver> observers = new();
		public bool IsWorkableTempreture { get; private set; }
		private int _tempreture;


		public void StartChecking()
		{
			Console.WriteLine("Датчик температуры начал проверку");
			_tempreture = 20;
			IsWorkableTempreture = true;
			while (IsWorkableTempreture)
			{
				Thread.Sleep(1000);
				CheckTepreture();
				_tempreture += 5;
			}
		}

		private void CheckTepreture()
		{
			Console.WriteLine($"Температура = {_tempreture}");
			if (_tempreture > 50)
			{
				Console.WriteLine("Температура превысила пороговое значение");
				IsWorkableTempreture = false;
				Notify();
			}
			else IsWorkableTempreture = true;
		}

		public void Attach(IObserver observer) => observers.Add(observer);

		public void Detach(IObserver observer) => observers.Remove(observer);

		public void Notify()
		{
			foreach (var observer in observers)
			{
				observer.Update(this);
			}
		}
	}

	public abstract class Robot : IObserver
	{
		private bool _isWorking;

		public bool IsWorking
		{
			get => _isWorking;
			set
			{
				if (value != _isWorking)
				{
					if (value == true) Console.WriteLine($"Робот {Model} начал свою работу");
					else Console.WriteLine($"Робот {Model} остановил свою работу");
				}
				_isWorking = value;
			}
		}
		public string Model { get; }
		public int Speed { get; }

		public Robot(string model, int speed)
		{
			Model = model;
			Speed = speed;
		}

		public void Update(ISubject subject)
		{
			if (subject is ProductionSystem ps)
			{
				IsWorking = ps.IsWorking;
			}
		}
	}

	public class ModifiedRobot : Robot
	{
		public ModifiedRobot() : base("R3000", 3000) { }
	}

	public class DeafaultRobot : Robot
	{
		public DeafaultRobot() : base("R100", 100) { }
	}

	public class Conveyor : IObserver
	{
		private bool _isWorking;

		public bool IsWorking
		{
			get => _isWorking;
			set
			{
				if (value != _isWorking)
				{
					if (value == true) Console.WriteLine($"Конвейер {Model} начал свою работу");
					else Console.WriteLine($"Конвейер {Model} остановил свою работу");
				}
				_isWorking = value;
			}
		}
		public string Model { get; }
		public int Speed { get; }

		public Conveyor(string model, int speed)
		{
			Model = model;
			Speed = speed;
		}

		public void Update(ISubject subject)
		{
			if (subject is ProductionSystem ps)
			{
				IsWorking = ps.IsWorking;
			}
		}
	}

	public class ModifiedConveyor : Conveyor
	{
		public ModifiedConveyor() : base("С3000", 3000) { }
	}

	public class DeafaultConveyor : Conveyor
	{
		public DeafaultConveyor() : base("С100", 100) { }
	}

	public interface IObserver
	{
		void Update(ISubject subject);
	}

	public interface ISubject
	{
		void Attach(IObserver observer);
		void Detach(IObserver observer);
		void Notify();
	}
}